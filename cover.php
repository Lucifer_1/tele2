<?php

ini_set("display_errors",1);
error_reporting(E_ALL);

require '/var/www/html/livecover/vendor/autoload.php';
require'config.php';

use Smit\Livecover\VK;


$VK = new VK($group_id_positive, $access_token);

$mysqli = new mysqli("localhost", "root", "root", "nerro");

if (mysqli_connect_errno()){
    $error = mysqli_connect_errno() . PHP_EOL . mysqli_connect_error();
    echo $error;
    exit;
}


$phones = $mysqli->query("SELECT phones FROM tele2;")->fetch_assoc()['phones'];
$comments = $mysqli->query("SELECT comments FROM tele2;")->fetch_assoc()['comments'];



$image = new Imagick("/var/www/html/projects/test/nerro/tele2/assets/tele_2.png");

$draw = new ImagickDraw();

$draw->setFont('/var/www/html/fonts/Roboto-Bold.ttf');

$draw->setFillColor('#ffffff');

$draw->setFontSize(58);

$image->annotateImage($draw, 1144, 170, -4, $phones);

$image->annotateImage($draw, 355, 238, -5, $comments);

$image->setImageFormat('png');
$file = "/var/www/html/projects/test/nerro/tele2/assets/tele_2_ready.png";
$image->writeImage($file);

$VK->changeCover($file);
